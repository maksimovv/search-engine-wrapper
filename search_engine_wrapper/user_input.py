from search_engine_wrapper import config


def correct_input_decorator(func):
    def correct_input_wrapper():
        is_process_done = False
        result = None
        while not is_process_done:
            result = func()
            if result is not None:
                is_process_done = True
            else:
                print('Bad input. Try again.')
        return result
    return correct_input_wrapper


def ask_search_query():
    print('Search the web')
    return input()


@correct_input_decorator
def ask_search_engine():
    print('Choose search engine:')
    for engine in config.SEARCH_ENGINES:
        print(config.SEARCH_ENGINES.get(engine), engine)
    user_input = parse_search_engine(input())
    return user_input


def parse_search_engine(user_input):
    try:
        user_input = int(user_input)
    except ValueError:
        user_input = None
    if user_input not in config.SEARCH_ENGINES:
        user_input = None
    return user_input


@correct_input_decorator
def ask_number_search_results():
    print('Number of search results:')
    user_input = parse_number_search_results(input())
    return user_input


def parse_number_search_results(user_input):
    try:
        user_input = int(user_input)
    except ValueError:
        user_input = None
    return user_input


@correct_input_decorator
def ask_recursive_search():
    print('Use recursive search:')
    print('Yes(y)', 'No(n)')
    user_input = parse_recursive_search(input())
    return user_input


def parse_recursive_search(user_input):
    user_input = user_input.lower()
    if user_input in ['yes', 'y']:
        user_input = True
    elif user_input in ['no', 'n']:
        user_input = False
    else:
        user_input = None
    return user_input


@correct_input_decorator
def ask_output_format():
    print('Choose output format:')
    for output in config.OUTPUT_FORMATS:
        print(config.OUTPUT_FORMATS.get(output), output)
    user_input = parse_output_format(input())
    return user_input


def parse_output_format(user_input):
    try:
        user_input = int(user_input)
    except ValueError:
        user_input = None
    if user_input not in config.OUTPUT_FORMATS:
        user_input = None
    return user_input
