from search_engine_wrapper import config


class Exporter:
    def __init__(self, format_id):
        self._format_id = format_id

    @staticmethod
    def _export_results_stdout(urls):
        print("Found urls:")
        for url in urls:
            print(f"{url} : {urls.get(url)}")
        print(f"Count of found urls: {len(urls)}")

    def export_results(self, urls):
        if self._format_id == config.STDOUT_ID:
            return self._export_results_stdout(urls)
        else:
            return None
