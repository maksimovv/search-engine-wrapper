import re
from urllib.parse import urlparse


class UrlFilter:

    def filter_wrapper(self, basic_url, new_url, query):
        """Filter-Wrapper for url """

        # Фильтр на валидную ссылку
        if not self.is_valid_filter(new_url):
            return False

        # Фильтр на нахождение в ссылке слов из запроса
        if not self.is_true_context_filter(new_url, query):
            return False

        # Фильтр на домен
        if self.is_same_domain_filter(basic_url, new_url):
            return False
        return True

    @staticmethod
    def is_same_domain_filter(url, href):
        """ Filter: is same domain name? """
        domain_name = urlparse(url).netloc
        return domain_name in href

    @staticmethod
    def is_valid_filter(url):
        """ Filter: is url valid? """
        parsed = urlparse(url)
        is_valid_scheme = bool(parsed.scheme) and parsed.scheme in ('ftp', 'https', 'http')
        is_valid_netloc = bool(parsed.netloc)
        return is_valid_scheme and is_valid_netloc

    @staticmethod
    def is_true_context_filter(contents, query):
        """Filter: is there the words-query on website-page / in url """
        for word in query.split():
            if len(re.findall(r"{0}".format(word.lower()), contents.lower())) == 0:
                return False
        return True
