from bs4 import BeautifulSoup
from urllib.parse import urlparse, urljoin, unquote
from search_engine_wrapper import config


class WebpageParser:
    def __init__(self, engine=0):
        self._engine_id = engine

    @staticmethod
    def _parse_duckduckgo_result_urls(contents):
        """ Parse duckduckgo search results. Return all result-urls"""
        soup = BeautifulSoup(contents, 'html.parser')
        urls = dict()
        for a_tag in soup.find_all('a', class_="result__a"):
            url = unquote(unquote(a_tag.get('href').split('//duckduckgo.com/l/?uddg=')[1]))
            urls[a_tag.text] = url
        return urls

    @staticmethod
    def _parse_website_urls(contents, basic_url):
        """ Parse any website-page for urls"""

        soup = BeautifulSoup(contents, "html.parser")
        website_urls = dict()

        for a_tag in soup.find_all('a'):
            href = a_tag.get('href')
            if href == "" or href is None:
                continue
            href = urljoin(basic_url, href)
            parsed_href = urlparse(href)
            href = parsed_href.scheme + "://" + parsed_href.netloc + parsed_href.path

            if href[-1] == '/':
                href = href[-1]
            website_urls[a_tag.text] = href
        return website_urls

    def parse_webpage(self, contents, basic_url=None):
        if self._engine_id == config.DDGO_ID:
            return self._parse_duckduckgo_result_urls(contents)
        else:
            return self._parse_website_urls(contents, basic_url)
