from search_engine_wrapper import config, parser, requester, url_filter


class UrlGetter:
    def __init__(self, engine=0):
        self._engine_id = engine
        self._parser = parser.WebpageParser(engine)
        self._requester = requester.Requester()
        self._filter = url_filter.UrlFilter()

    def _get_duckduckgo_urls(self, query):
        """ Get all result-urls for duckduckgo"""
        url = config.URL_DUCKDUCKGO.format(query)
        response = self._requester.get_website_content(url)
        if response is None:
            return None
        result = self._parser.parse_webpage(response)
        return result

    def _get_website_urls(self, query, url):
        """ Get filtered urls for any website, if website's context is OK"""
        clear_urls = dict()
        website_urls = dict()

        # Делаем запрос
        response = self._requester.get_website_content(url)
        if response is None:
            return None

        # Фильтр на контекст страницы - если есть упоминания слов из запроса на странице - то идем парсить ссылки
        if self._filter.is_true_context_filter(response, query):
            website_urls = self._parser.parse_webpage(response, url)

        # Фильтр на полученные ссылки
        for new_name, new_url in website_urls.items():
            if self._filter.filter_wrapper(url, new_url, query):
                clear_urls[new_name] = new_url

        return clear_urls

    def _get_website_urls_deep(self, website, query, max_urls, found_urls):
        """ Recursive wrapper for get_urls_from_website()"""

        # Идем на сайт, проверяем контекст сайта, если все ок - забираем оттуда ссылки
        urls = self.get_urls(query, website)
        if urls is None:
            return

        # Добавляем ссылки к результатам
        found_urls = self._update_dict(found_urls, urls)

        # Проверяем достачно ли ссылок на данный момент
        if len(found_urls) >= max_urls:
            return found_urls

        # Начинаем перебирать новые найденные ссылки
        for url_name, url in urls.items():
            self._get_website_urls_deep(url, query, max_urls, found_urls)

        return found_urls

    @staticmethod
    def _update_dict(dict_to_upd, new_dict):
        for url_name, url in new_dict.items():
            dict_to_upd[url_name] = url
        return dict_to_upd

    def get_urls_recursive(self, res_urls_dict, query, min_count):
        """Going through res_urls recursive"""
        init_res_urls = dict(res_urls_dict)
        count = 0

        for res_name, res_url in init_res_urls.items():
            if not len(res_urls_dict) < min_count:
                break
            print(f"Deep search in {res_url}")
            new_urls = self._get_website_urls_deep(res_url, query, min_count, res_urls_dict)
            if new_urls is not None:
                res_urls_dict = self._update_dict(res_urls_dict, new_urls)
            count += 1
            print(f"Current length: {len(res_urls_dict)}")
            print(f"Visited {count} web search results of {len(init_res_urls)}")

        return res_urls_dict

    def get_urls(self, query, url=None):
        """ Wrapper for all get_urls function """
        if self._engine_id == config.DDGO_ID:
            return self._get_duckduckgo_urls(query)
        else:
            return self._get_website_urls(query, url)
