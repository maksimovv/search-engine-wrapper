import requests

visited_urls = list()


class Requester:

    @staticmethod
    def _decode_response(response):
        try:
            response_content = response.content.decode('utf-8')
        except UnicodeDecodeError:
            response_content = None
        return response_content

    @staticmethod
    def _request_wrapper(url):
        headers = {'user-agent': 'search-engine-wrapper'}
        try:
            response = requests.get(url=url, headers=headers, timeout=5)
            response.encoding = 'utf-8'
        except (requests.exceptions.ReadTimeout,
                requests.exceptions.ConnectTimeout,
                requests.exceptions.ConnectionError):
            return None
        return response

    def get_website_content(self, url):
        """ Wrapper for requests.get()"""
        if url in visited_urls:
            return None

        visited_urls.append(url)
        response = self._request_wrapper(url)
        if response is None:
            return None
        if response.status_code != 200:
            return None
        return self._decode_response(response)
