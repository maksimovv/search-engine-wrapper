import sys
import search_engine_wrapper as sew


if __name__ == '__main__':
    # Задаем параметры для поиска
    min_count = None
    query = sew.ask_search_query()
    search_engine = 1
    is_recursive_search = sew.ask_recursive_search()
    if is_recursive_search:
        min_count = sew.ask_number_search_results()
    output_format = 1

    # Получаем список ссылок-результов от поисковика
    url_getter = sew.UrlGetter(search_engine)
    res_urls = url_getter.get_urls(query)
    if res_urls is None:
        print('Get error while get urls from search-engine')
        sys.exit()

    # Начинаем рекурсивный поиск ссылок по сайтам, найденным поисковиком
    if is_recursive_search:
        url_getter_any = sew.UrlGetter()
        found_urls = url_getter_any.get_urls_recursive(res_urls, query, min_count)
    else:
        found_urls = res_urls

    # Экспорт результатов
    exporter = sew.Exporter(output_format)
    exporter.export_results(found_urls)
