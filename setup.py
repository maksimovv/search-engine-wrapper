import os
from setuptools import setup, find_packages


# allow setup.py to be run from any path
os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))

setup(
    name='sew',
    version='0.1',
    packages=find_packages(),
    include_package_data=True,
    license='GNU General Public License v3.0',
    description='Search engine wrapper',
    # long_description=README,
    # url='https://github.com/DanteOnline/charon',
    author='maksimovv',
    author_email='v.maksimov@ctm.ru',
    keywords=['web', 'search'],
    # classifiers = [],
    entry_points={
        'console_scripts': [
            'sew = search_engine_wrapper.main:main',
        ]
    },
)